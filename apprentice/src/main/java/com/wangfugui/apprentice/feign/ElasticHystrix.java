package com.wangfugui.apprentice.feign;

import com.alibaba.fastjson.JSONObject;

import com.wangfugui.apprentice.common.util.ResponseUtils;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Service;

@Service
public class ElasticHystrix implements FallbackFactory<ElasticSearchApi> {


    @Override
    public ElasticSearchApi create(Throwable cause) {
        return new ElasticSearchApi() {
            @Override
            public Object insertData(String index, String id, JSONObject jsonObject) {
                return ResponseUtils.error();
            }

            @Override
            public Object indexSearch(String index, JSONObject jsonObject) {
                return ResponseUtils.error();

            }

            @Override
            public Object aliasesSearch(String aliases, JSONObject jsonObject) {
                return ResponseUtils.error();

            }

            @Override
            public Object insertAliases(String index, JSONObject jsonObject) {
                return ResponseUtils.error();
            }

            @Override
            public Object checkIndex(String index) {
                return ResponseUtils.success("0");
            }

            @Override
            public Object updateData(String index, String id, JSONObject jsonObject) {
                return ResponseUtils.error();
            }

            @Override
            public Object getDataByIndex(String index, String id) {
                return ResponseUtils.msg("没有找到数据");
            }

            @Override
            public Object indices() {
                return null;
            }

            @Override
            public Object reindex(JSONObject jsonObject) {
                return null;
            }

            @Override
            public Object delIndex(String index) {
                return null;
            }

            @Override
            public Object newIndex(String index) {
                return null;
            }

            @Override
            public Object mapping(String index, JSONObject jsonObject) {
                return null;
            }

            @Override
            public Object updateAliases(JSONObject jsonObject) {
                return null;
            }

            @Override
            public Object analyze(JSONObject jsonObject) {
                return null;
            }
        };
    }
}
