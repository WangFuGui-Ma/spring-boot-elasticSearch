package com.wangfugui.apprentice.feign;

import com.alibaba.fastjson.JSONObject;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author MaSiyi
 * @version 1.0.0 2022/1/19
 * @since JDK 1.8.0
 */
@FeignClient(name = "EIP-MAPP-SEARCH-SERVER", url = "${elsearch.url}", fallbackFactory = ElasticHystrix.class)
public interface ElasticSearchApi {

    /** 插入数据
     * @Param: [index, id, jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @PostMapping("/{index}/_doc/{id}")
    Object insertData(@PathVariable(name = "index") String index, @PathVariable(name = "id") String id, @RequestBody JSONObject jsonObject);

    /** 索引搜索
     * @Param: [index, jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @GetMapping("/{index}/_search")
    Object indexSearch(@PathVariable(name = "index") String index, @RequestBody JSONObject jsonObject);

    /** 别名搜索
     * @Param: [aliases, jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @GetMapping("/{aliases}/_search")
    Object aliasesSearch(@PathVariable(name = "aliases") String aliases, @RequestBody JSONObject jsonObject);

    /** 插入别名
     * @Param: [index, jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @PutMapping("/{index}")
    Object insertAliases(@PathVariable(name = "index") String index, @RequestBody JSONObject jsonObject);

    /** 检查索引是否存在
     * @Param: [index]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @RequestMapping(value = "/{index}", method = RequestMethod.HEAD)
    Object checkIndex(@PathVariable(name = "index") String index);

    /** 更新字段
     * @Param: [index, id, jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @PostMapping("/{index}/_doc/{id}/_update")
    Object updateData(@PathVariable(name = "index") String index, @PathVariable(name = "id") String id, @RequestBody JSONObject jsonObject);

    /** 获取数据
     * @Param: [index, id]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @GetMapping("/{index}/_doc/{id}")
    Object getDataByIndex(@PathVariable(name = "index") String index, @PathVariable(name = "id") String id);

    /** 查询所有索引信息
     * @Param: []
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @GetMapping("/_cat/indices?format=json")
    Object indices();

    /** 复制索引
     * @Param: [jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @GetMapping("/_reindex")
    Object reindex(@RequestBody JSONObject jsonObject);

    /** 删除索引
     * @Param: [index]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @DeleteMapping("/{index}")
    Object delIndex(@PathVariable(name = "index") String index);

    /** 新建索引
     * @Param: [index]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @PutMapping("/{index}")
    Object newIndex(@PathVariable(name = "index") String index);

    /** 指定字段信息
     * @Param: [index, jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @PutMapping("/{index}/_mapping")
    Object mapping(@PathVariable(name = "index") String index, @RequestBody JSONObject jsonObject);


    /** 更新别名
     * @Param: [jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @PostMapping("/_aliases")
    Object updateAliases(@RequestBody JSONObject jsonObject);


    /** 分词
     * @Param: [jsonObject]
     * @return: java.lang.Object
     * @Author: MaSiyi
     * @Date: 2022/3/7
     */
    @PostMapping("/_analyze")
    Object analyze(@RequestBody JSONObject jsonObject);




}
