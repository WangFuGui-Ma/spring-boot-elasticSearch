package com.wangfugui.apprentice.feign;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class ElasticService {

    @Autowired
    private ElasticSearchApi elasticSearchApi;

    /**
     * 插入
     *
     * @Param: []
     * @return: void
     * @Author: MaSiyi
     * @Date: 2022/3/25
     */
    private void insertElasticSearch() {


        JSONObject jsonObject = new JSONObject();

        String businessKey = "";

        AtomicReference<String> nowTaskName = new AtomicReference<>("");
        AtomicReference<String> assigneeName = new AtomicReference<>("");
        jsonObject.put("taskUpdateTime", System.currentTimeMillis());
        AtomicReference<JSONObject> assignee = new AtomicReference<>();


        //当前处理
        jsonObject.put("nowTaskName", nowTaskName.get());
        jsonObject.put("assigneeName", assigneeName.get());

        //下个处理人
        jsonObject.put("assignee", assignee.get());

        //表名称
        String mainTable = "";
        jsonObject.put("mainTable", mainTable);

        //插入数据
        elasticSearchApi.insertData(mainTable, businessKey, jsonObject);


    }

    public void indexSearch() {
        String indexName = "";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("size", 1);
        Object search = elasticSearchApi.indexSearch(indexName, jsonObject);

    }

    private void aliasesSearch() {
        JSONObject jsonObject = new JSONObject();


        JSONObject mustJson = new JSONObject();

        // "bool"封装
        JSONObject boolJson = new JSONObject();
        boolJson.put("bool", mustJson);
        jsonObject.put("query", boolJson);
        //排除返回字段
        JSONObject excludes = new JSONObject();
        excludes.put("excludes", "elasticData");
        jsonObject.put("_source", excludes);
        //微服务名称
        String serveName = "";
        Object search = elasticSearchApi.aliasesSearch(serveName, jsonObject);
    }

    public void insertAliases() {
        String mainTable = "";
        JSONObject aliases = new JSONObject();
        JSONObject aliase = new JSONObject();
        JSONObject nullJson = new JSONObject();
        aliase.put("serverName", nullJson);
        aliases.put("aliases", aliase);
        elasticSearchApi.insertAliases(mainTable, aliases);
    }

    public void checkIndex() {
        if (elasticSearchApi.checkIndex("mainTable") != null) {
            //do something
        }
    }

    public void updateElasticSearch() {

        JSONObject jsonObject = new JSONObject();
        JSONObject docJson = new JSONObject();
        docJson.put("taskUpdateTime", "taskTime");
        docJson.put("elasticData", "value");
        jsonObject.put("doc", docJson);
        elasticSearchApi.updateData("mainTable", "id", jsonObject);
    }

    public void getDataByIndex() {

        Object dataByIndex = elasticSearchApi.getDataByIndex("mainTable", "id");
    }

    public void indices() {

        Object indices = elasticSearchApi.indices();
    }

    public void reindex() {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("index", "oldIndex");
        JSONObject jsonObject2 = new JSONObject();
        String newIndex = "oldIndex" + "_copy";
        jsonObject2.put("index", newIndex);
        JSONObject jsonObjectAll = new JSONObject();
        jsonObjectAll.put("source", jsonObject);
        jsonObjectAll.put("dest", jsonObject2);
        //1.先将原索引复制到新索引中
        elasticSearchApi.reindex(jsonObjectAll);
        //2.删除旧索引
        elasticSearchApi.delIndex("oldIndex");
        //3.新建原来的索引
        elasticSearchApi.newIndex("oldIndex");
        //4.修改索引字段mapper

        String s1 = "{\n" +
                "    \"properties\": {\n" +
                "        \"elasticData\": {\n" +
                "            \"type\": \"text\",\n" +
                "            \"analyzer\":\"ik_max_word\"\n" +
                "        }\n" +
                "    }\n" +
                "}";

        elasticSearchApi.mapping("oldIndex", JSONObject.parseObject(s1));
        //7.为所有索引添加别名
        String s2 = "{\n" +
                "    \"actions\": [\n" +
                "        {\n" +
                "            \"add\": {\n" +
                "                \"index\": \"*\",\n" +
                "                \"alias\": \"eip-mapp-oa-server\"\n" +
                "            }\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        elasticSearchApi.updateAliases(JSONObject.parseObject(s2));
    }

    public void analyze(String keyWord) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("analyzer", "ik_max_word");
        jsonObject.put("text", keyWord);
        elasticSearchApi.analyze(jsonObject);
    }


}
