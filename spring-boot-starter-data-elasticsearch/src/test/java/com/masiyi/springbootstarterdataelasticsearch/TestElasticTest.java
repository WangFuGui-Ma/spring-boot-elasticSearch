package com.masiyi.springbootstarterdataelasticsearch;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.masiyi.springbootstarterdataelasticsearch.doman.ElasticTest;
import com.masiyi.springbootstarterdataelasticsearch.mapper.ElasticTestMapper;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.document.Document;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @Author masiyi
 * @Date 2023/6/14 13:51
 * @PackageName:com.masiyi.springbootstarterdataelasticsearch
 * @ClassName: TestElasticTest
 * @Description: TODO
 * @Version 1.0
 */
@SpringBootTest
public class TestElasticTest {

    @Autowired
    ElasticTestMapper elasticTestMapper;
    @Autowired
    ElasticsearchRestTemplate elasticsearchTemplate;
    @Autowired
    RestClient restClient;

    /**
     * 増
     */
    @Test
    void save() {
        ElasticTest elasticTest = new ElasticTest();
        elasticTest.setName("李四");
        elasticTest.setAge(23);
        elasticTest.setIsMan(true);
        elasticsearchTemplate.save(elasticTest);
    }

    @Test
    void insert() {
        ElasticTest elasticTest = new ElasticTest();
        elasticTest.setName("李四");
        elasticTest.setAge(23);
        elasticTest.setIsMan(true);
        elasticTestMapper.save(elasticTest);
    }

    @Test
    void insertId() {
        ElasticTest elasticTest = new ElasticTest();
        elasticTest.setName("掉头发的王富贵");
        elasticTest.setAge(25);
        elasticTest.setIsMan(true);
        elasticTest.setId(2434235L);
        elasticTestMapper.save(elasticTest);
    }

    @Test
    void saveAll() {
        ElasticTest elasticTest = new ElasticTest();
        elasticTest.setName("李四");
        elasticTest.setAge(24);
        elasticTest.setIsMan(true);
        elasticTestMapper.saveAll(Arrays.asList(elasticTest));
    }

    /**
     * 删
     */
    @Test
    void delete() {
        ElasticTest elasticTest = new ElasticTest();
        elasticTest.setId(2342342L);
        elasticTestMapper.delete(elasticTest);
    }

    @Test
    void deleteById() {
        elasticTestMapper.deleteById(2342342L);
    }

    @Test
    void deleteAll() {
        elasticTestMapper.deleteAll();
    }

    /**
     * 改
     */
    @Test
    void update() {
        ElasticTest elasticTest = new ElasticTest();
        elasticTest.setName("掉头发的王富贵");
        elasticTest.setId(2434234L);
        elasticTestMapper.save(elasticTest);
    }

    @Test
    void updateAll() {
        ElasticTest elasticTest = new ElasticTest();
        elasticTest.setName("掉头发的王富贵");
        elasticTest.setAge(24);
        elasticTest.setIsMan(true);
        elasticTest.setId(2434234L);
        elasticTestMapper.save(elasticTest);
    }

    @Test
    void updateNow() {
        ElasticTest elasticTest = elasticTestMapper.findById(2434234L).get();
        elasticTest.setName("不掉头发的王富贵");
        elasticTestMapper.save(elasticTest);
    }

    @Test
    void updateNow2() {
        ElasticTest elasticTest = new ElasticTest();
        elasticTest.setName("掉头发的王富贵h");
        Map map = JSONObject.parseObject(JSONObject.toJSONString(elasticTest), Map.class);
        UpdateQuery updateQuery = UpdateQuery.builder("0ZUv7okBcQy9f7u_tXkH").withDocument(Document.from(map)).build();
        elasticsearchTemplate.update(updateQuery, IndexCoordinates.of("elastic_test"));
    }

    /**
     * 查
     */
    @Test
    void select() {
        Optional<ElasticTest> byId = elasticTestMapper.findById(2434234L);
        byId.ifPresent(System.out::println);
    }

    @Test
    void findAllById() {
        Iterable<ElasticTest> allById = elasticTestMapper.findAllById(Arrays.asList(2434234L));
        allById.forEach(System.out::println);
    }

    @Test
    void findAll() {
        Iterable<ElasticTest> allById = elasticTestMapper.findAll();
        allById.forEach(System.out::println);
    }

    @Test
    void findByAge() {
        ElasticTest byAge = elasticTestMapper.findByAge(24);
        System.out.println(byAge);
    }

    @Test
    void findAllByAge() {
        List<ElasticTest> allByAge = elasticTestMapper.findAllByAge(24);
        allByAge.forEach(System.out::println);
    }

    @Test
    void findAllSort() {
        Sort age = Sort.by("age").ascending();
        Iterable<ElasticTest> all = elasticTestMapper.findAll(age);
        all.forEach(System.out::println);
    }

    @Test
    void findAllSortDE() {
        Sort age = Sort.by("age").descending();
        Iterable<ElasticTest> all = elasticTestMapper.findAll(age);
        all.forEach(System.out::println);
    }

    @Test
    void findAllPage() {
        PageRequest pageRequest = PageRequest.of(0, 10);
        Page<ElasticTest> all = elasticTestMapper.findAll(pageRequest);
        all.forEach(System.out::println);
        System.out.println(JSON.toJSONString(all));
    }

    @Test
    void findTopByAge() {
        ElasticTest topByAge = elasticTestMapper.findTopByAge(24);
        System.out.println(topByAge);
    }

    @Test
    void findMyStyle() {
        TermQueryBuilder termQueryBuilder = new TermQueryBuilder("name.keyword", "掉头发的王富贵");
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(termQueryBuilder);

        SearchHits<ElasticTest> search = elasticsearchTemplate.search(nativeSearchQuery, ElasticTest.class);
        List<SearchHit<ElasticTest>> hitList = search.getSearchHits();
        for (SearchHit<ElasticTest> hit : hitList) {
            ElasticTest entity = hit.getContent(); // 获取实体对象
            System.out.println(entity);
            String index = hit.getIndex(); // 获取索引名
            System.out.println(index);

        }
    }


}
