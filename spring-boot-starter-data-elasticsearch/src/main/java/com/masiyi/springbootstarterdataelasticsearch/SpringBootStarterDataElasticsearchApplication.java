package com.masiyi.springbootstarterdataelasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootStarterDataElasticsearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootStarterDataElasticsearchApplication.class, args);
	}

}
