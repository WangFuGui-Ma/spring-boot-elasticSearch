package com.masiyi.springbootstarterdataelasticsearch.mapper;

import com.masiyi.springbootstarterdataelasticsearch.doman.ElasticTest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface ElasticTestMapper extends ElasticsearchRepository<ElasticTest,Long> {

    ElasticTest findByAge(Integer age);

    List<ElasticTest> findAllByAge(Integer age);

    ElasticTest findTopByAge(Integer age);
}
