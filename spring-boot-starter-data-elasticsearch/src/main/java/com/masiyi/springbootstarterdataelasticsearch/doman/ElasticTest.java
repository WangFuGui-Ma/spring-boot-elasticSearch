package com.masiyi.springbootstarterdataelasticsearch.doman;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.WriteTypeHint;

/**
 * @Author masiyi
 * @Date 2023/6/14 13:46
 * @PackageName:com.masiyi.springbootstarterdataelasticsearch.doman
 * @ClassName: ElasticTest
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Document(indexName = "elastic_test",writeTypeHint = WriteTypeHint.FALSE)
public class ElasticTest {

    private Long id;

    private String name;

    private Integer age;

    private Boolean isMan;
}
